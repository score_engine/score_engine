@extends('front/layout')

@section('content')
	<div class="container">
		<div class="col-lg-8">
			<h1>{{ $event->name }}</h1>
			<p>{{ $event->description }}</p>
		</div>
		<div class="col-lg-4">
			<h1>Details</h1>
			<p><b>Start Time:</b> {{ $event->start_time }}</p>
			<p><b>End Time:</b> {{ $event->end_time }}</p>
			<p><b>Blue Team Capacity:</b> X of {{ $event->bt_capacity }}</p>
			<p><b>Red Team Capacity:</b> X of {{ $event->rt_capacity }}</p>
		</div>
	</div>
@stop