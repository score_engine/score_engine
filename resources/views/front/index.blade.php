@extends('front/layout')

@section('content')
	<div class="container">
		<div class="col-md-6 col-sm-12">
			<h1>Upcoming Events</h1>
			@if ($upcoming_events->count() == 0)
				There are no upcoming events scheduled at this time.
			@endif

			@foreach ($upcoming_events as $event)
				<h2><a href="{{ route('event.show', $event->id) }}">{{ $event->name }}</a></h2>	
				<p>{{ $event->description }}</p>
			@endforeach
		</div>

		<div class="col-md-6 col-sm-12">
			<h1>Ongoing Events</h1>
			@if ($ongoing_events->count() ==0)
				There are no ongoing events happening at this time.
			@endif
			@foreach ($ongoing_events as $event)
				<h2><a href="{{ route('event.show', $event->id) }}">{{ $event->name }}</a></h2>
				<p>{{ $event->description }}</p>
			@endforeach
		</div>
	</div>
@stop