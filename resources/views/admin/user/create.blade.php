@extends('admin/layout')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<h2>Users</h2>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			{!! Form::open(['route' => 'admin.user.store', 'method' => 'POST']) !!}
				<legend>Create User</legend>
				<div class="form-group">
					{!! Form::label('username', 'Username', ['class' => 'control-label']) !!}
					{!! Form::text('username', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
					{!! Form::text('email', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('password', 'Password', ['class' => 'control-label']) !!}
					{!! Form::password('password', ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('password_confirmation', 'Confirm Password', ['class' => 'control-label']) !!}
					{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('admin', 'Is Admin?', ['class' => 'control-lable']) !!}
					{!! Form::radio('admin', '1', false) !!} Yes
					{!! Form::radio('admin', '0', true) !!} No
				</div>

				{!! Form::submit('Save', ['class' => 'btn btn-default']) !!}
			{!! Form::close() !!}
		</div>
	</div>
@stop