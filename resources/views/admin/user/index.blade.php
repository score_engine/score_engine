@extends('admin/layout')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<h2>
				Users
				<a href="{{ route('admin.user.create') }}" class="btn btn-success pull-right">New User</a>
			</h2>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<table class="table table-striped table-bordered">
				<thead>
					<th>Username</th>
					<th>Email</th>
					<th>Is Admin?</th>
					<th>Actions</th>
				</thead>
				<tbody>
					@foreach ($users as $user)
						<tr>
							<td>{{ $user->username }}</td>
							<td>{{ $user->email }}</td>
							<td>{{ $v = ($user->admin == 1) ? 'Yes' : 'No' }}</td>
							<td>
								<a href="{{ route('admin.user.edit', $user->id) }}" class="btn btn-primary btn-xs">Edit</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@stop