<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Wrapid's Scoring Engine Admini Panel</title>
	<link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('bower_components/metisMenu/dist/metisMenu.min.css') }}">
	<link rel="stylesheet" href="{{ asset('bower_components/startbootstrap-sb-admin-2/dist/css/sb-admin-2.css') }}">
	<link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
</head>
<body>
	<div id="wrapper">
		<div class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;">
			<div class="navbar-header"></div>
			<div class="navbar-default sidebar" role="navigation">
				<div class="sidebar-nav navbar-collapse">
					<ul class="nav" id="side-menu">
						<li><a href="{{ url('/') }}">Home</a></li>
						<li><a href="{{ route('admin.user.index') }}">Users</a></li>
						<li><a href="{{ route('admin.server.index') }}">Servers</a></li>
						<li><a href="{{ route('admin.event.index') }}">Events</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div id="page-wrapper">
			@yield('content')
		</div>
	</div>
	<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
	<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('bower_components/metisMenu/dist/metisMenu.min.js') }}"></script>
	<script src="{{ asset('bower_components/startbootstrap-sb-admin-2/dist/js/sb-admin-2.js') }}"></script>
	<script src="{{ asset('bower_components/moment/min/moment.min.js') }}"></script>
	<script src="{{ asset('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
	@yield('script')
</body>
</html>