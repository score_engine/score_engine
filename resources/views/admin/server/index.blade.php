@extends('admin/layout')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<h2>Servers <a href="{{ route('admin.server.create') }}" class="btn btn-success pull-right">New Server</a> </h2>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<table class="table table-striped table-bordered">
				<thead>
					<th>ID</th>
					<th>Address</th>
					<th>HostName</th>
					<th>Name</th>
					<th>Status</th>
					<th>Actions</th>
				</thead>
				<tbody>
					@foreach ($servers as $server)
						<tr>
							<td>{{ $server->id }}</td>
							<td>{{ $server->address }}</td>
							<td>{{ $server->hostname }}</td>
							<td>{{ $server->name }}</td>
							<td>
								@if ($server->ping() == true)
									<button class="btn-xs btn btn-success">Online</button>
								@else
									<button class="btn-xs btn btn-danger">Offline</button>
								@endif
							</td>
							<td>
								<a href="{{ route('admin.server.edit', $server->id) }}" class="btn btn-xs btn-primary">Edit</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@stop