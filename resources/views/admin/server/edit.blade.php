@extends('admin/layout')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<h2>Servers</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			{!! Form::model($server, ['route' => ['admin.server.update', $server->id], 'method' => 'PUT']) !!}
				<legend>
					Edit Server
					@if ($server->ping())
						<button class="btn btn-xs btn-success">Online</button>
					@else
						<button class="btn btn-xs btn-danger">Offline</button>
					@endif
				</legend>
				<div class="form-group">
					{!! Form::label('address', 'Address', ['class' => 'control-label']) !!}
					{!! Form::text('address', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('hostname', 'HostName', ['class' => 'control-label']) !!}
					{!! Form::text('hostname', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
					{!! Form::text('name', null, ['class' => 'form-control']) !!}
				</div>

				{!! Form::submit('Save', ['class' => 'btn btn-default']) !!}
			{!! Form::close() !!}
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<h2>
				Services
				<a href="{{ route('admin.server.service.create', $server->id) }}" class="btn btn-xs btn-success pull-right">New Service</a>
			</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<table class="table table-striped table-bordered">
				<thead>
					<th>Type</th>
					<th>Username</th>
					<th>Password</th>
					<th>Port</th>
					<th>URL</th>
					<th>Status</th>
					<th>Actions</th>
				</thead>
				@foreach ($services as $service)
					<tr>
						<td>{{ $service->serviceType->name }}</td>
						<td>{{ $service->username }}</td>
						<td>{{ $service->password }}</td>
						<td>{{ $service->port }}</td>
						<td>{{ $service->url }}</td>
						<td>
							@if ($service->check())
								<button class="btn btn-xs btn-success">Online</button>
							@else
								<button class="btn btn-xs btn-danger">Offline</button>
							@endif
						</td>
						<td>
							<a href="{{ route('admin.server.service.edit', [$server->id, $service->id]) }}" class="btn btn-xs btn-primary">Edit</a>
						</td>
					</tr>
				@endforeach
			</table>
		</div>
	</div>
@stop