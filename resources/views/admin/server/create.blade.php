@extends('admin/layout')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<h2>Servers</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			{!! Form::open(['route' => 'admin.server.store', 'method' => 'POST']) !!}
				<legend>New Server</legend>
				<div class="form-group">
					{!! Form::label('address', 'Address', ['class' => 'control-label']) !!}
					{!! Form::text('address', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('hostname', 'HostName', ['class' => 'control-label']) !!}
					{!! Form::text('hostname', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
					{!! Form::text('name', null, ['class' => 'form-control']) !!}
				</div>

				{!! Form::submit('Save', ['class' => 'btn btn-default']) !!}
			{!! Form::close() !!}
		</div>
	</div>
@stop