@extends('admin/layout')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<h2>Services</h2>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			{!! Form::open(['route' => ['admin.server.service.store', $server], 'method' => 'POST']) !!}
				<legend>New Service</legend>
				<div class="form-group">
					{!! Form::label('service_type_id', 'Service Type', ['class' => 'control-label']) !!}
					{!! Form::select('service_type_id', $service_types, null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('username', 'Username', ['class' => 'control-label']) !!}
					{!! Form::text('username', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('password', 'Password', ['class' => 'control-label']) !!}
					{!! Form::text('password', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('url', 'URL', ['class' => 'control-label']) !!}
					{!! Form::text('url', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('port', 'Port Number', ['class' => 'control-label']) !!}
					{!! Form::text('port', null, ['class' => 'form-control']) !!}
				</div>

				{!! Form::submit('Save', ['class' => 'btn btn-default']) !!}
			{!! Form::close() !!}
		</div>
	</div>
@stop