@extends('admin/layout')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<h2>Events</h2>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			{!! Form::open(['route' => 'admin.event.store', 'method' => 'POST']) !!}
				<legend>New Event</legend>
				<div class="form-group">
					{!! Form::label('server_id', 'Server', ['class' => 'control-label']) !!}
					{!! Form::select('server_id', $servers, null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
					{!! Form::text('name', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
					{!! Form::textarea('description', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('start_time', 'Start Time', ['class' => 'control-label']) !!}
					{!! Form::text('start_time', null, ['class' => 'form-control datetimepicker']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('end_time', 'End Time', ['class' => 'control-label']) !!}
					{!! Form::text('end_time', null, ['class' => 'form-control datetimepicker']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('bt_start_time', 'Blue Team Start Time', ['class' => 'control-label']) !!}
					{!! Form::text('bt_start_time', null, ['class' => 'form-control datetimepicker']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('rt_start_time', 'Red Team Start Time', ['class' => 'control-label']) !!}
					{!! Form::text('rt_start_time', null, ['class' => 'form-control datetimepicker']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('bt_capacity', 'Blue Team Capacity', ['class' => 'control-label']) !!}
					{!! Form::text('bt_capacity', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('rt_capacity', 'Red Team Capacity', ['class' => 'control-label']) !!}
					{!! Form::text('rt_capacity', null, ['class' => 'form-control']) !!}
				</div>

				{!! Form::submit('Save', ['class' => 'btn btn-default']) !!}
			{!! Form::close() !!}
		</div>
	</div>
@stop

@section('script')
	<script>
	$(document).ready(function() {
		$('.datetimepicker').datetimepicker({

		});
	});
	</script>
@stop