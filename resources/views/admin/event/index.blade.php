@extends('admin/layout')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<h2>
				Events
				<a href="{{ route('admin.event.create') }}" class="btn btn-success pull-right">New Event</a>
			</h2>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<table class="table table-striped table-bordered">
				<thead>
					<th>Name</th>
					<th>Server</th>
					<th>Start</th>
					<th>End</th>
					<th>Actions</th>
				</thead>
				<tbody>
					@foreach ($events as $event)
						<tr>
							<td>{{ $event->name }}</td>
							<td>{{ $event->server->name }}</td>
							<td>{{ $event->start_time }}</td>
							<td>{{ $event->end_time }}</td>
							<td>
								<a href="{{ route('admin.event.edit', $event->id) }}" class="btn btn-xs btn-primary">Edit</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@stop