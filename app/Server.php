<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Service;

class Server extends Model
{
	/**
	 * The attributes available for mass assignment.
	 *
	 * @var array
	 **/
	protected $fillable = [
		'address',
		'hostname',
		'name',
	];

	/**
	 * A Server has Many Services
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function services()
	{
		return $this->hasMany(Service::class);
	}

	/**
	 * Ping the server to see if it is up. If ICMP is disabled this will fail to ascertain the status.
	 *
	 * @return boolean
	 */
	public function ping()
	{
		$output = [];
		exec('ping -c 1 ' . $this->address, $output);

		if (!isset($output[4])) {
			return false;
		}

		if (strpos($output[4], '100% packet loss') !== false) {
			return false;
		}
		
		return true;
	}
}