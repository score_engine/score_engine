<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Service;

class ServiceType extends Model
{
	/**
	 * The attributes available for mass assignment.
	 *
	 * @var array
	 **/
	protected $fillable = [
		'name',
	];

	/**
	 * A ServiceType has many Services.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function services()
	{
		return $this->hasMany(Service::class);
	}
}