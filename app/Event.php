<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Service;
use App\Server;

class Event extends Model
{
	/**
	 * The attributes available for mass assignment.
	 *
	 * @var array
	 **/
	protected $fillable = [
		'bt_start_time',
		'rt_start_time',
		'description',
		'bt_capacity',
		'rt_capacity',
		'start_time',
		'server_id',
		'end_time',
		'name',
	];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 **/
	protected $dates = [
		'bt_start_time',
		'rt_start_time',
		'created_at',
		'updated_at',
		'start_time',
		'end_time',
	];

	/**
	 * An Event belongs to a Server.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function server()
	{
		return $this->belongsTo(Server::class);
	}

	/**
	 * An Event has many Services.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function services()
	{
		return $this->hasMany(Service::class);
	}

	/**
	 * Scope a query to only include upcoming events.
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeUpcoming($event)
	{
		return $event->where('start_time', '>', Carbon::now());
	}

	/**
	 * Scope a query to only include ongoing events.
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeOngoing($event)
	{
		return $event->where('start_time', '<', Carbon::now())
			->where('end_time', '>', Carbon::now());
	}
}
