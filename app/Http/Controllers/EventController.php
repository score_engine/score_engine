<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Event;

class EventController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$events = Event::all();

		return view('front/event/index', compact('events'));
	}

	/**
	 * Display a single resource.
	 *
	 * @return Response
	 */
	public function show($event)
	{
		$event = Event::findOrFail($event);

		return view('front/event/show', compact('event'));
	}
}