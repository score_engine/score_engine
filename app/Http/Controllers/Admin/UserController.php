<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::all();

		return view('admin/user/index', compact('users'));
	}

	/**
	 * Display a template for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin/user/create');
	}

	/**
	 * Display a template for editing an existing resource.
	 *
	 * @return Response
	 */
	public function edit($user)
	{
		$user = User::findOrFail($user);

		return view('admin/user/edit')->with(['user' => $user]);
	}

	/**
	 * Create a new resource.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'username' => 'required',
			'password' => 'required|confirmed',
			'email'    => 'required|email',
		]);

		$input = $request->input();

		$user = User::create($input);

		return redirect()->route('admin.user.index');
	}

	/**
	 * Update an existing resource.
	 *
	 * @return Response
	 */
	public function update($user, Request $request)
	{
		$this->validate($request, [
			'username' => 'required',
			'password' => 'confirmed',
			'email'    => 'required|email',
		]);

		$input = $request->input();

		$user = User::findOrFail($user);
		$user->update($input);

		return redirect()->route('admin.user.index');
	}

	/**
	 * Delete an existing resource.
	 *
	 * @return Response
	 */
	public function destroy()
	{
		User::destroy($user);

		return redirect()->route('admin.user.index');
	}
}