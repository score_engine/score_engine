<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ServiceType;
use App\Service;
use App\Server;

class ServiceController extends Controller
{
	/**
	 * Display a template for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($server)
	{
		$service_types = ServiceType::lists('name', 'id');

		return view('admin/server/service/create',
			compact(
				'service_types',
				'server'
			)
		);
	}

	/**
	 * Display a template for editing an existing resource.
	 *
	 * @return Response
	 */
	public function edit($server, $service)
	{
		$service = Service::find($service);
		$service_types = ServiceType::lists('name', 'id');

		return view('admin/server/service/edit',
			compact(
				'service_types',
				'service',
				'server'
			)
		);
	}

	/**
	 * Create a new resource.
	 *
	 * @return Response
	 */
	public function store($server, Request $request)
	{
		$server = Server::findOrFail($server);
		$input = $request->input();

		$service = new Service($input);
		$server->services()->save($service);

		return redirect()->route('admin.server.edit', $server);
	}

	/**
	 * Update an existing resource.
	 *
	 * @return Response
	 */
	public function update($server, $service, Request $request)
	{
		$service = Service::findOrFail($service);
		$input = $request->input();

		$service->update($input);

		return redirect()->route('admin.server.edit', $server);
	}
}
