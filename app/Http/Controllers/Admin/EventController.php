<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Server;
use App\Event;

class EventController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$events = Event::with('server')->get();

		return view('admin/event/index', compact('events'));
	}

	/**
	 * Display a template for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$servers = Server::lists('name', 'id');

		return view('admin/event/create', compact('servers'));
	}

	/**
	 * Display a template for editing an existing event.
	 *
	 * @return Response
	 */
	public function edit($event)
	{
		$event = Event::findOrFail($event);
		$servers = Server::lists('name', 'id');

		return view('admin/event/edit',
			compact(
				'servers',
				'event'
			)
		);
	}

	/**
	 * Create a new resource.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$input = $request->input();

		Event::create($input);

		return redirect()->route('admin.event.index');
	}

	/**
	 * Update an existing resource.
	 *
	 * @return Response
	 */
	public function update($event, Request $request)
	{
		$input = $request->input();
		$event = Event::findOrFail($event);

		$event->update($input);

		return redirect()->route('admin.event.index');
	}

	/**
	 * Destroy an existing resource.
	 *
	 * @return Response
	 */
	public function destroy($event)
	{
		Event::destroy($event);

		return redirect()->route('admin.event.index');
	}
}
