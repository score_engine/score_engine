<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Server;

class ServerController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$servers = Server::all();

		return view('admin/server/index', compact('servers'));
	}

	/**
	 * Display a template for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin/server/create');
	}

	/**
	 * Display a template for editing an existing resource.
	 *
	 * @return Response
	 */
	public function edit($server)
	{
		$server = Server::findOrFail($server);
		$services = $server->services;

		return view('admin/server/edit',
			compact(
				'server',
				'services'
			)
		);
	}

	/**
	 * Create a new resource.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$input = $request->input();

		Server::create($input);

		return redirect()->route('admin.server.index');
	}

	/**
	 * Update an existing resource.
	 *
	 * @return Response
	 */
	public function update($server, Request $request)
	{
		$server = Server::findOrFail($server);
		$input = $request->input();

		$server->update($input);

		return redirect()->back();
	}

	/**
	 * Delete an existing resource
	 *
	 * @return Response
	 */
	public function destroy($server)
	{
		Server::destroy($servr);

		return redirect()->back();
	}
}
