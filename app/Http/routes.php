<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function()
{
	Route::resource('server', 'ServerController');
	Route::resource('server.service', 'ServiceController', ['except' => ['index', 'show']]);
	Route::resource('event', 'EventController');
	Route::resource('user', 'UserController');
});

Route::get('/', function() {
	$upcoming_events = \App\Event::upcoming()->get();
	$ongoing_events = \App\Event::ongoing()->get();

	return view('front/index',
		compact(
			'upcoming_events',
			'ongoing_events'
		)
	);
});

Route::resource('event', 'EventController');
