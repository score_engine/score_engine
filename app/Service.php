<?php namespace App;

use phpseclib\Net\SSH2;
use Illuminate\Database\Eloquent\Model;
use App\ServiceType;
use App\Server;

class Service extends Model
{
	/**
	 * The attributes available for mass assignment.
	 *
	 * @var array
	 **/
	protected $fillable = [
		'service_type_id',
		'username',
		'password',
		'port',
		'url',
	];

	/**
	 * A Service belongs to a Server.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function server()
	{
		return $this->belongsTo(Server::class);
	}

	/**
	 * A Service has many Statuses
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function statuses()
	{
		return $this->hasMany(Status::class);
	}

	/**
	 * A Service has a ServiceType
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function serviceType()
	{
		return $this->belongsTo(ServiceType::class);
	}

	/**
	 * Check the service using the appropriate method dictated by the Service's ServiceType
	 *
	 * @return boolean
	 */
	public function check()
	{
		return $this->{$this->serviceType->name}();
	}

	/**
	 * Test if ssh is working given the services attributes.
	 *
	 * @return boolean
	 */
	private function ssh()
	{
		$con = null;

		if (!empty($this->username)) {
			$con = new SSH2($this->server->address, $this->port);
			if (@$con->login($this->username, $this->password)) {
				return true;
			} else {
				return false;
			}
		
	return true;
		}
	}

	/**
	 * Test if ftp is working given the service attributes.
	 *
	 * @return boolean
	 */
	public function ftp()
	{	
		$con = null;

		if (!($con = ftp_connect($this->server->address, $this->port, 5))) {
			return false;
		} else {
			if (@ftp_login($con, $this->username, $this->password)) {
				return true;
			} else {
				return false;
			}

		}

		return false;
	}

	/**
	 * Test if the HTTP server is working given the service attributes.
	 *
	 * @return boolean
	 */
	public function http()
	{
		$url = $this->url;

		$c_init = curl_init($url);
		curl_setopt($c_init, CURLOPT_CONNECTTIMEOUT, 3);
		curl_setopt($c_init, CURLOPT_HEADER, true);
		curl_setopt($c_init, CURLOPT_NOBODY, true);
		curl_setopt($c_init, CURLOPT_RETURNTRANSFER, true);

		curl_exec($c_init);
		$resp = curl_getinfo($c_init, CURLINFO_HTTP_CODE);
		curl_close($c_init);

		if ($resp == 200 || $resp == 301) {
			return true;
		}

		return false;
	}
}
