<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Service;
use App\Event;

class Status extends Model
{
	/**
	 * The attributes available for mass assignment.
	 *
	 * @var array
	 **/
	protected $fillable = [
		'status'
	];

	/**
	 * A Status belongs to a Service
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function service()
	{
		return $this->belongsTo(Service::class)
	}

	/**
	 * A Status belongs to an Event
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function event()
	{
		return $this->belongsTo(Event::class);
	}
}
