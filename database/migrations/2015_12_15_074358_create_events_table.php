<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('server_id');
            $table->timestamp('start_time');
            $table->timestamp('bt_start_time');
            $table->timestamp('rt_start_time');
            $table->timestamp('end_time');

            $table->string('name');
            $table->text('description');
            $table->integer('bt_capacity');
            $table->integer('rt_capacity');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
