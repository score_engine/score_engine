<?php

use App\ServiceType;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(ServiceTypeSeeder::class);

        Model::reguard();
    }
}

class ServiceTypeSeeder extends Seeder
{
    /**
     * Add supported service types to the database.
     *
     * @return void
     */
    public function run()
    {
        $types = ['ftp', 'ssh', 'http'];

        foreach ($types as $type) {
            ServiceType::create(['name' => $type]);
        }
    }
}