## Score Engine

Score engine is a web application to help manage Red vs. Blue competitions by handling event information, server information, and checking the status of various services that should remain alive during the competitions.

## Installing

For additional information and dependencies check the Laravel website.

Clone this repository and run `composer install`.